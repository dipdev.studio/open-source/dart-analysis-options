# Dart Analysis Options

Options for DipDev Studio Flutter projects

## Getting Started

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

- Need to add the dependency to the project

```yaml
 dev_dependencies:
  pedantic: ^1.8.0+1
```

- After that, you need to add submodule of dart analysis options

```cmd
git submodule add -b analysis_options https://gitlab.com/dipdev.studio/open-source/dart-analysis-options.git analysis_options
```

- You must create a symbolic link to the configuration file

### Linux

```cmd
ln -s analysis_options/analysis_options.yaml analysis_options.yaml
```

### Windows

```cmd
mklink "analysis_options.yaml" "analysis_options\analysis_options.yaml" 
```

## Authors

> This project developed by [**DipDev Studio**](https://dipdev.studio) Team: [@Dimoshka](https://www.linkedin.com/in/dmytroprylutskyi/)
